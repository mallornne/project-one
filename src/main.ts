import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import Toast from 'vue-toastification';
import 'vue-toastification/dist/index.css';
import firebase from 'firebase';

//import VueRandomColor from 'vue-randomcolor';

//Vue.use(VueRandomColor);
Vue.config.productionTip = false;
Vue.use(Toast);

const firebaseConfig = {
	apiKey: 'AIzaSyAgKa1FQF442mIT1OSZKN9O7ZgMncJFQP8',
	authDomain: 'test-8ba85.firebaseapp.com',
	projectId: 'test-8ba85',
	storageBucket: 'test-8ba85.appspot.com',
	messagingSenderId: '827628311604',
	appId: '1:827628311604:web:6392fd39d7813b3706a661',
	measurementId: 'G-P8L864R8C7',
	databaseURL: 'https://test-8ba85-default-rtdb.europe-west1.firebasedatabase.app',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

new Vue({
	router,
	store,
	vuetify,
	render: (h) => h(App),
}).$mount('#app');
