export class InterpretData {
	id: string;
	interpret: string;
	song: string;
	URLLink?: string;
	// ? Optional
	rating: number;
	constructor(data?: Partial<InterpretData>) {
		Object.assign(this, data);
	}
}
