export class Menik {
	text: string;
	path: string;
	icon?: string;
	constructor(data?: Partial<Menik>) {
		Object.assign(this, data);
	}
}
