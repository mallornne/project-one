export class UserData {
	firstname: string;
	middlename: string;
	lastname: string;
	email: string;
	password?: string;
	constructor(data?: Partial<UserData>) {
		Object.assign(this, data);
	}
}
