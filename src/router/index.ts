import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';
import About from '../views/About.vue';
import Test from '../views/Test1.vue';
import Profil from '../views/Profil.vue';
import { routerTypes } from './router-types';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: '/',
		name: 'Home',
		component: Home,
	},
	{
		path: '/about',
		name: routerTypes.ABOUT,
		component: About,
	},
	{
		path: '/test1',
		name: routerTypes.TEST1,
		component: Test,
	},
	{
		path: '/profil',
		name: routerTypes.PROFIL,
		component: Profil,
	},
	{
		path: '/*',
		name: routerTypes.NOT_FOUND,
		component: Home,
	},
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
