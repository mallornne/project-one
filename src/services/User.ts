import { UserData } from '@/models';
import FirebaseService from './FireBase';

export enum TablesNamesUser {
	USERS = 'users',
}

export default class UsersService extends FirebaseService {
	static getUserInfo(id: string): Promise<UserData> {
		return this.getData(`${TablesNamesUser.USERS}/${id}`);
	}

	static getActualUser() {
		return this.actualUser();
	}

	static getUsers(): Promise<any> {
		return this.getData(`${TablesNamesUser.USERS}`);
	}

	static createUser(id: string, user: UserData) {
		return this.setData(`${TablesNamesUser.USERS}/${id}`, user);
	}

	static updateUserById(id: string, user: Partial<UserData>) {
		return this.setData(`${TablesNamesUser.USERS}/${id}`, user);
	}

	static removeUserById(id: string) {
		return this.removeData(`${TablesNamesUser.USERS}/${id}`);
	}
}
