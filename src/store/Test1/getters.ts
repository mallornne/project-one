import { InterpretData } from '@/models';
import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { getterTypes, Test1State } from './types';

export const getters: GetterTree<Test1State, RootState> = {
	[getterTypes.INTERPRETSDATA](state): InterpretData[] {
		return state.interpretsData;
	},
};
