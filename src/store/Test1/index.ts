import { Module } from 'vuex';
import { RootState } from '../types';
import { Test1State } from './types';
import { actions } from '@/store/Test1/actions';
import { mutations } from '@/store/Test1/mutations';
import { getters } from '@/store/Test1/getters';

export const state: Test1State = {
	interpretsData: [{ id: '1', interpret: 'test', song: 'sdfa', rating: 2 }],
};

export const test1: Module<Test1State, RootState> = {
	namespaced: true,
	state,
	actions,
	mutations,
	getters,
};
