import { InterpretData } from '@/models';
import { MutationTree } from 'vuex';
import { mutationTypes, Test1State } from './types';

export const mutations: MutationTree<Test1State> = {
	[mutationTypes.INSERTINTERPRETDATA](state, payload: InterpretData) {
		state.interpretsData.push(payload);
	},
	[mutationTypes.REMOVEINTERPRETDATA](state, payload: string) {
		state.interpretsData = state.interpretsData.filter((i) => i.id !== payload);
	},
};
