import { InterpretData } from '@/models';

export interface Test1State {
	interpretsData: InterpretData[];
}

export const actionTypes = {};

export const mutationTypes = {
	INSERTINTERPRETDATA: 'insertInterpretData',
	REMOVEINTERPRETDATA: 'removeInterpretData',
};

export const getterTypes = {
	INTERPRETSDATA: 'interpretsData',
};
