import { UserData } from '@/models';
import FirebaseService from '@/services/FireBase';
import UsersService from '@/services/User';
import { ActionTree } from 'vuex';
import { RootState } from '../types';
import { actionTypes, mutationTypes, UserDataState } from './types';

export const actions: ActionTree<UserDataState, RootState> = {
	async [actionTypes.REGISTRATIONUSER]({ commit, state }, payload: UserData) {
		await FirebaseService.createUserWithEmailAndPassword(payload.email, payload.password!).then(
			async (response) => {
				if (response.user) {
					delete payload.password;
					await UsersService.createUser(response.user.uid, payload)
						.then(() => {
							FirebaseService.sendVerificationMail();
							commit(mutationTypes.SET_USER_INFO, payload.email);
						})
						.catch((error) => {
							throw new Error(error);
						});
				} else {
					throw new Error();
				}
			}
		);
	},
};
