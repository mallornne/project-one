import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { getterTypes, UserDataState } from './types';

export const getters: GetterTree<UserDataState, RootState> = {
	[getterTypes.ISLOGED](state): boolean {
		return state.email !== '' ? true : false;
	},
	[getterTypes.USEREMAIL](state): string {
		return state.email;
	},
};
