import { Module } from 'vuex';
import { RootState } from '../types';
import { UserDataState } from './types';
import { actions } from '@/store/UserData/actions';
import { mutations } from '@/store/UserData/mutations';
import { getters } from '@/store/UserData/getters';

export const state: UserDataState = {
	users: [],
	email: '',
};

export const userdata: Module<UserDataState, RootState> = {
	namespaced: true,
	state,
	actions,
	mutations,
	getters,
};
