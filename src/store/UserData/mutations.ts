import { UserData } from '@/models';
import { MutationTree } from 'vuex';
import { mutationTypes, UserDataState } from './types';

export const mutations: MutationTree<UserDataState> = {
	[mutationTypes.INSERTUSERDATA](state, payload: UserData) {
		state.users.push(payload);
	},
	[mutationTypes.SET_USER_INFO](state, payload: string) {
		state.email = payload ?? '';
	},
};
