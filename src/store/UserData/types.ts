import { UserData } from '@/models';

export interface UserDataState {
	users: UserData[];
	email: string;
}

export const actionTypes = {
	REGISTRATIONUSER: 'registrationUser',
};

export const mutationTypes = {
	INSERTUSERDATA: 'insertUserData',
	SET_USER_INFO: 'setUserInfo',
};

export const getterTypes = {
	ISLOGED: 'isLoged',
	USEREMAIL: 'userEmail',
};
