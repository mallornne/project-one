import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { actionTypes, getterTypes, mutationTypes, RootState } from './types';
import { test1 } from '@/store/Test1';
import { userdata } from '@/store/UserData';
import { Menik } from '@/models';
//import VuexPersistence from 'vuex-persist';

Vue.use(Vuex);

//const vuexLocal = new VuexPersistence<RootState>({
//	storage: window.localStorage,
//});

export const store: StoreOptions<RootState> = {
	state: {
		name: `Pato`,
		surname: '',
		values: ['A', 'B', 'C', 'D'],
		menus: [
			{ text: 'Home', path: '/', icon: 'fa-home' },
			{ text: 'Test1', path: '/test1', icon: 'fa-vial' },
			{ text: 'About', path: '/about', icon: 'fa-book' },
			{ text: 'Profil', path: '/profil', icon: 'fa-user' },
		],
	},

	getters: {
		[getterTypes.FULL_NAME](state): string {
			//const fullName = `${state.name} ${state.surname ?? ''}`; `dfajsfkasdjf ůdfja sdfja sůfjasdlk sdklůf alkdfja sfkl ${name}`;
			return state.name + ' ' + (state.surname ?? '');
		},
		[getterTypes.NAME](state): string {
			return state.name;
		},
		[getterTypes.SURNAME](state): string {
			return state.surname;
		},
		[getterTypes.VALUES](state): string[] {
			return state.values;
		},
		[getterTypes.MENUS](state): Menik[] {
			return state.menus;
		},
	},

	mutations: {
		[mutationTypes.CHANGE_NAME](state, payload: string) {
			state.name = payload;
		},
		[mutationTypes.CHANGE_SURNAME](state, payload: string) {
			state.surname = payload;
		},
	},

	actions: {
		[actionTypes.CHANGE_NAME](state, payload: string) {
			this.commit(mutationTypes.CHANGE_NAME, payload);
		},
		[actionTypes.CHANGE_SURNAME](state, payload: string) {
			this.commit(mutationTypes.CHANGE_SURNAME, payload);
		},
	},

	modules: {
		test1,
		userdata,
	},

	//plugins: [vuexLocal.plugin],
};

export default new Vuex.Store<RootState>(store);
