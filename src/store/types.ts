import { Menik } from '@/models';

export interface RootState {
	name: string;
	surname: string;
	values: string[];
	menus: Menik[];
}

export const moduleTypes = {
	TEST1: 'test1',
	USERDATA: 'userdata',
	//odkazuje na types.ts ve složce
};

export const actionTypes = {
	CHANGE_NAME: 'changeName',
	CHANGE_SURNAME: 'changeSurname',
};

export const mutationTypes = {
	CHANGE_NAME: 'changeName',
	CHANGE_SURNAME: 'changeSurname',
};

export const getterTypes = {
	FULL_NAME: 'fullName',
	VALUES: 'values',
	NAME: 'name',
	SURNAME: 'surname',
	MENUS: 'menik',
};
