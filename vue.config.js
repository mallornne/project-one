module.exports = {
	transpileDependencies: ['vuetify'],
	devServer: {
		https: false,
		port: 8099,
		host: '127.0.0.1',
		disableHostCheck: true,
	},
};
